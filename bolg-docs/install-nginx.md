ngnix安装配置
=======
1. 解压blog-docs/software/nginx-1.12.2.zip到安装目录
   ![Alt text](./snapshot/install-nginx-01.png)
2. 域名配置
   ![Alt text](./snapshot/install-nginx-04.png)
3. 打开nginx安装目录下的conf/nginx.conf配置文件
   ![Alt text](./snapshot/install-nginx-02.png)
4. 启动、停止、重启nginx
   ![Alt text](./snapshot/install-nginx-03.png)
5. 使用配置的域名访问
