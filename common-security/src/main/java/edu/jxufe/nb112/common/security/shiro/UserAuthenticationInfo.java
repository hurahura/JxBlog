/*******************************************************************************
 * (c) Copyright 2017 lvxi. All Rights Reserved.
 ******************************************************************************/

package edu.jxufe.nb112.common.security.shiro;

import edu.jxufe.nb112.common.code.security.mode.IUserAuth;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.util.ByteSource;

/**
 * Created by lvxi on 2017/2/12.
 */
public class UserAuthenticationInfo  extends SimpleAuthenticationInfo {
    private IUserAuth userAuth;
    public UserAuthenticationInfo(IUserAuth userAuth, Object principal, Object hashedCredentials, ByteSource credentialsSalt, String realmName) {
        super(principal, hashedCredentials, credentialsSalt, realmName);
        this.userAuth =userAuth;
    }

    public IUserAuth getUserAuth() {
        return userAuth;
    }

    public void setUserAuth(IUserAuth userAuth) {
        this.userAuth = userAuth;
    }
}
