package edu.jxufe.nb112.common.security.service;

import edu.jxufe.nb112.common.code.security.mode.IUser;
import edu.jxufe.nb112.common.code.security.mode.IUserAuth;

/**
 * Created by lvxi on 2018/3/16.
 * Note:
 */
public interface SecurityService {
    public IUserAuth findUserAuthByAccount(String account);
    public IUser findUserByAccount(String account);

}
