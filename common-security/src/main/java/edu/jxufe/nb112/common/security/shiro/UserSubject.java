/*******************************************************************************
 * (c) Copyright 2017 lvxi. All Rights Reserved.
 ******************************************************************************/

package edu.jxufe.nb112.common.security.shiro;

import edu.jxufe.nb112.common.code.security.mode.IUser;
import org.apache.shiro.web.subject.support.WebDelegatingSubject;

/**
 * Created by lvxi on 2017/2/12.
 */
public class UserSubject extends WebDelegatingSubject {
    private IUser user;

    public UserSubject(WebDelegatingSubject subject,  boolean sessionEnabled,IUser user) {
        super(subject.getPrincipals(), subject.isAuthenticated(), subject.getHost(), subject.getSession(), sessionEnabled , subject.getServletRequest(), subject.getServletResponse(), subject.getSecurityManager());
        this.user = user;
    }

    public IUser getUser() {
        return user;
    }

    public void setUser(IUser user) {
        this.user = user;
    }
}
