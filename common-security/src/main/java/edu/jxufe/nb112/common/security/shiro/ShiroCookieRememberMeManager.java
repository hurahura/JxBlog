package edu.jxufe.nb112.common.security.shiro;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by lvxi on 2018/3/15.
 * Note:
 */
public class ShiroCookieRememberMeManager extends CookieRememberMeManager{

    private Integer defaultMaxAge= 3*24*60*60; //3天

    private String  maxAgeParamName="maxAge";

    @Override
    protected void rememberSerializedIdentity(Subject subject, byte[] serialized){
        HttpServletRequest request = WebUtils.getHttpRequest(subject);
        String maxAgeStr = request.getParameter(getMaxAgeParamName());
        Cookie cookie = getCookie();
        //Integer maxAge = defaultMaxAge;
        if(StringUtils.isNumeric(maxAgeStr)){
            cookie.setMaxAge(Integer.parseInt(maxAgeStr));
        }
        super.rememberSerializedIdentity(subject,serialized);
    }

    public Integer getDefaultMaxAge() {
        return defaultMaxAge;
    }

    public void setDefaultMaxAge(Integer defaultMaxAge) {
        this.defaultMaxAge = defaultMaxAge;
    }

    public String getMaxAgeParamName() {
        return maxAgeParamName;
    }

    public void setMaxAgeParamName(String maxAgeParamName) {
        this.maxAgeParamName = maxAgeParamName;
    }



}
