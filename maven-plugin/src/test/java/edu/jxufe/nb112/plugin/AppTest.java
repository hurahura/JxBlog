package edu.jxufe.nb112.plugin;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        File file = new File("E:\\workspace\\IdeaProject\\JxBlog\\test-plugin\\src\\main\\webapp\\WEB-INF\\pages\\jsp\\common\\head.jsp");
        InputStream in = null;
        String staticRoot = "E:/workspace/IdeaProject/JxBlog/blog-web/src/main/webapp/WEB-INF/static";
        Map<String, String> paramsMap = new HashMap<String, String>();
        System.out.println(staticRoot);
        paramsMap.put("ctx", staticRoot);
        try {
            in = new FileInputStream(file);
            String context = IOUtils.toString(in);
            System.out.println(context);
            //匹配 link和script 标签
            String searchTagPatternStr = "(?<!<[!%]?--[%]?[>]?)<[\\s]*(link|script)[\\s\\S]*?/?[\\s]*[>]+?";
            Pattern searchTagPattern = Pattern.compile(searchTagPatternStr, Pattern.MULTILINE);
            Matcher searchTagMatcher = searchTagPattern.matcher(context);
            //匹配 href和src
            String hrefPatternStr = "(?<=(href|src)=\")[^\"]*?(?=\")";
            Pattern hrefPattern = Pattern.compile(hrefPatternStr, Pattern.MULTILINE);

            StringBuffer resultBuffer = new StringBuffer();
            while (searchTagMatcher.find()) {
                System.out.println("Found value: " + searchTagMatcher.group());
                int end = searchTagMatcher.end(), beg = searchTagMatcher.start();
                String staticTag = context.substring(beg, end);
                System.out.println("str(" + beg + "," + end + ")=" + staticTag);
                if (!isCommentBlock(context, "<!--", "-->", beg, end) && !isCommentBlock(context, "<%--", "--%>", beg, end)) {
                    Matcher hrefMatcher = hrefPattern.matcher(staticTag);
                    if (hrefMatcher.find()) {
                        System.out.println("提取到链接" + hrefMatcher.group(1) + "  => " + hrefMatcher.group());
                        String href = hrefMatcher.group().trim();
                        if (!href.startsWith("http") && !href.startsWith("HTTP")) {
                            String replaceElhref = reaplaceEL(paramsMap, hrefMatcher.group()).trim();
                            System.out.println("修正链接：" + replaceElhref);
                            System.out.println("提取文件特征MD5：");
                            File linkFile = new File(replaceElhref);
                            if (linkFile.exists()) {
                                try {
                                    String md5 = DigestUtils.md5Hex(new FileInputStream(linkFile));
                                    System.out.println(md5);
                                    int idx = href.indexOf('?');
                                    if(idx<0){
                                        href=href+"?"+md5;
                                    }else {
                                        href=href.substring(0,idx)+'?'+md5;
                                    }
                                    StringBuffer sb = new StringBuffer();
                                    hrefMatcher.appendReplacement(sb, href.replaceAll("\\$", "\\\\\\$"));  //将替换后的子串以及其之前到上次匹配子串之后的字符串段添加到一个StringBuffer对象里。
                                    hrefMatcher.appendTail(sb);
                                    System.out.println("替换后"+sb);
                                    searchTagMatcher.appendReplacement(resultBuffer,sb.toString().replaceAll("\\$", "\\\\\\$"));

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                System.out.println(linkFile.getAbsoluteFile()+"文件不存在");
                            }
                        }
                    }
                } else {
                    System.out.println("是注释");
                }

            }
            searchTagMatcher.appendTail(resultBuffer);
            System.out.println("结果：----------------------");
            System.out.println(resultBuffer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(in);
        }
    }


    public static boolean isCommentBlock(String str, String openComment, String closeComment, int begin, int end) {
        int bc = str.lastIndexOf(openComment, begin);
        int ec = str.lastIndexOf(closeComment, begin);
        if (bc == -1) {
            //System.out.println("无注释ok");
            return false;
        } else {
            if (ec == -1) {
                //System.out.println("无注释ok");
                return false;

            } else {
                if (bc > ec) {
                    return true;
                    /*
                    int suec = str.indexOf("-->",end);
                    if(suec==-1){
                        //System.out.println("注释错误漏掉-->");
                        return true;
                    }else {
                        //System.out.println("被注释" + str.substring(bc, suec + 3));
                        return true;
                    }*/
                } else {
                    //System.out.println("其他注释ok:"+str.substring(bc, ec+3));
                    return false;
                }
            }
        }
    }

    public static String reaplaceEL(Map<String, String> paramsMap, String str) {
        Set<Map.Entry<String, String>> entrySet = paramsMap.entrySet();
        Iterator<Map.Entry<String, String>> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            System.out.println(entry.getValue());
            System.out.println("\\${.*?" + entry.getKey() + ".*?}");
            str = str.replaceAll("\\$\\{.*?" + entry.getKey() + ".*?\\}", entry.getValue().replaceAll("\\$", "\\\\\\$"));
        }
        return str;
    }


}
