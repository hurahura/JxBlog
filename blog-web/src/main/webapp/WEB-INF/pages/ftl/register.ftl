<head>
    <title>用户注册</title>
    <style>
        @media (min-width: 769px){
            #mainBody{
                margin-top: 100px;
                margin-bottom: 100px;
            }
        }
    </style>
    <script type="text/javascript" footer>
        $(document).ready(function () {
            $("#registerForm").bootstrapValidator({
                live: 'disabled',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    account: {
                        validators: {
                            notEmpty: {
                                message: '请输入要注册的账号。'
                            },
                            regexp: {
                                regexp: /^[_A-z0-9]{4,30}$/,
                                message: '用户名要是英文字母,数字,下划线_,长度4至30位。'
                            }
                        }
                    },
                    email:{
                        validators: {
                            notEmpty: {
                                message: '请输入要注册的邮箱。'
                            },
                            emailAddress: {
                                message: '邮箱格式不正确。'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {},
                            stringLength: {
                                min: 6,
                                max: 30
                            }
                        }
                    },
                    confirmPassword: {
                        validators: {
                            notEmpty: {},
                            identical: {
                                field: 'password',
                                message: '两次输入的密码不一致。'
                            }
                        }
                    }
                }
            });

            validateFieldOnline("#registerForm","account","/account/hasAccount",{
                validReturnValue:false,
                message:"该账号已经被注册了。"
            });
            validateFieldOnline("#registerForm","email","/account/hasEmail",{
                validReturnValue:false,
                message:"该邮箱已经被注册了。"
            });
            formAutoSubmit("#registerForm",register);
            $("#register-btn").click(register);
        });

        function  register() {
            ajaxSubmitForm("#registerForm",{
                url:ctx+"/register",
                closeMask:false,
                subsuccess:function(data) {
                    successTipBox("注册成功",function () {
                        closeLoadLayer();
                        window.location.href=ctx;
                    });
                }
            })
        }
    </script>
</head>
<body>
    <div class="col-xs-offset-1 col-xs-10 col-sm-offset-4 col-sm-4">
        <form id="registerForm" class="form-horizontal" method="post">
            <div class="text-center ms-form-title mb10" >新用户注册</div>

            <div class="form-group">
                <input type="text" class="form-control" id="account" name="account"  placeholder="用户名">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" id="email" name="email" placeholder="邮箱">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="password" name="password" placeholder="密码">
            </div>

            <div class="form-group">
                <input type="password" class="form-control not-submit" id="confirmPassword" name="confirmPassword" placeholder="确认密码">
            </div>
            <div class="ms-title-border">
                <div>
                    <span style="font-size: 10px">更多信息</span>
                </div>
            </div>
            <div>

            </div>

            <div class="form-group">
                  <button id="register-btn" type="button" class="btn  btn-info btn-submit" style="width: 100%">立即注册</button>
            </div>
        </form>
    </div>
</div>
</body>
