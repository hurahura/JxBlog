<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common/base.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <title>用户登录</title>
    <style>
        @media (min-width: 769px){
            #mainBody{
                margin-top: 100px;
                margin-bottom: 100px;
            }
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginForm").bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    account: {
                        validators: {
                            notEmpty: {
                                message: '请输入要账号或验证的邮箱'
                            },
                            regexp: {
                                regexp: /^[_A-z0-9]{4,30}$|^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
                                message: '用户名要是英文字母,数字,下划线_,长度4至30位,邮箱'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {},
                            stringLength: {
                                min: 6,
                                max: 30
                            }
                        }
                    }
                }
            });
            formAutoSubmit("#loginForm",login);
        });

        function  login() {
            ajaxSubmitForm("#loginForm",{
                url:ctx+"/login",
                closeMask:false,
                subsuccess:function(data) {
                    successTipBox("登录成功",function () {
                        closeLoadLayer();
                        window.location.href=ctx;
                    });
                }
            })
        }
        function toggleRememberMe() {
            var $rms = $("#rememberSlt");
            $rms.is(":visible") && $rms.hide()|| $rms.show() ;
        }

    </script>
</head>
<body>
    <div class="col-xs-offset-1 col-xs-10 col-sm-offset-4 col-sm-4 col-lg-offset-5 col-lg-2">
        <form id="loginForm" class="form-horizontal" method="post">
                <div class="text-center ms-form-title mb10">用户登录</div>

                <div class="form-group">
                    <input type="text" class="form-control" id="account" name="account"  placeholder="用户名/邮箱">
                </div>
                <div class="form-group mb8">
                    <input type="password" class="form-control" id="password" name="password" placeholder="密码">
                </div>

                <div class="form-group">
                    <input type="checkbox" onclick="toggleRememberMe(this)" name="rememberMe"> 自动登录
                    <select id="rememberSlt" name="maxAge" style="display: none">
                        <option value="259200">3天</option>
                        <option value="604800">7天</option>
                        <option value="2592000">30天</option>
                        <option value="5184000">60天</option>
                        <option value="10368000">120天</option>
                        <option value="31536000">365天</option>
                    </select>
                </div>

                <div class="form-group">
                      <button type="button" onclick="login()" class="btn  btn-info btn-submit" style="width: 100%">登  录</button>
                </div>

                <div class="form-group text-center">
                    没有账号? <a href="/register" >立即注册</a>
                </div>
        </form>
</div>


</body>
</html>
