/*******************************************************************************
 * (c) Copyright 2017 lvxi. All Rights Reserved.
 ******************************************************************************/

package edu.jxufe.nb112.blog.web.controller;

import edu.jxufe.nb112.common.code.exception.ExceptionCodeConstant;
import edu.jxufe.nb112.common.code.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("test")
public class TestController extends BaseController{
    private final static Logger LOG = LoggerFactory.getLogger(TestController.class.getName());
    @RequestMapping
    public String index(){
        return "test";
    }

    @RequestMapping("exception")
    public String exception(){
        throw new ServiceException( ExceptionCodeConstant.DEFAULT,"测试");
    }

}
