/*******************************************************************************
 * (c) Copyright 2017 lvxi. All Rights Reserved.
 ******************************************************************************/

package edu.jxufe.nb112.blog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class IndexController extends BaseController{
    private final static Logger LOG = LoggerFactory.getLogger(IndexController.class.getName());

    @RequestMapping
    public String index(){
        return "index";
    }

}
