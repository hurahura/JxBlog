/*******************************************************************************
 * (c) Copyright 2017 lvxi. All Rights Reserved.
 ******************************************************************************/

package edu.jxufe.nb112.blog.web.controller;


import edu.jxufe.nb112.blog.core.persist.entity.User;
import edu.jxufe.nb112.common.security.shiro.UserSubject;
import edu.jxufe.nb112.common.web.servlet.tags.CRSFTokenTag;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by lvxi on 2017/2/6.
 */
public class BaseController {

    @Autowired
    private HttpSession session;

    @Autowired
    private HttpServletRequest request;

    //@Autowired
   // private HttpServletResponse response;

    /**
     * 获取当前登录人的信息
     * @return
     */
    protected User getCurrentUserProfile() {
        UserSubject userSubject = getUserSubject();
        Assert.notNull(userSubject,"用户未登录");
        return (User)userSubject.getUser();
    }

    /**
     * 是否登录验证过
     * @return
     */
    protected boolean isLogined(){
        return SecurityUtils.getSubject().isAuthenticated();
    }

    /**
     * 获取当前用户id,没有登录返回null
     * @return
     */
    protected Long getCurrentUserId(){
        if(isLogined()){
            return getCurrentUserProfile().getId();
        }
        return null;
    }

    protected UserSubject getUserSubject() {
        if(SecurityUtils.getSubject()instanceof  UserSubject){
            return ((UserSubject) SecurityUtils.getSubject());
        }
        return null;
    }

    /**
     * 获取当前登录人的ip
     * @return
     * @throws Exception
     */
    public  String getIpAddr() throws Exception {
        String ip = request.getHeader("X-Real-IP");
        if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
            return ip;
        }
        ip = request.getHeader("X-Forwarded-For");
        if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
            // 多次反向代理后会有多个IP值，第一个为真实IP。
            int index = ip.indexOf(',');
            if (index != -1) {
                return ip.substring(0, index);
            } else {
                return ip;
            }
        } else {
            return request.getRemoteAddr();
        }
    }

    /**
     * 获取CRSFToken
     * @param id 页面标识符
     * @return
     */
    public String getCRSFToken(String id){
        return (String) getSession().getAttribute("CRSFToken_"+id);
    }

    /**
     * 产生CRSFToken
     * @param id
     * @return
     */
    public String generateCRSFToken(String id){
       return CRSFTokenTag.generateRandomCRSFToken(request.getSession(),id);
    }
    /**
     * CRSFToken验证
     * @param id
     * @return
     */
    public boolean validateCRSFToken(String id){
        String  answer = request.getParameter("CRSFToken");
        if(StringUtils.isNoneEmpty(answer)){
            String randomCRSFToken = (String) request.getSession().getAttribute("_CRSFToken_"+id);
            if(answer.equals(randomCRSFToken)) {
                return true;
            }
        }
        return false;
    }

    public String redirect(String url){
        return "redirect:/"+url;
    }
    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
}
