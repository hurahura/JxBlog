package edu.jxufe.nb112.blog.web.support;

import edu.jxufe.nb112.blog.core.persist.service.user.UserAuthService;
import edu.jxufe.nb112.blog.core.persist.service.user.UserService;
import edu.jxufe.nb112.common.code.security.mode.IUser;
import edu.jxufe.nb112.common.code.security.mode.IUserAuth;
import edu.jxufe.nb112.common.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by lvxi on 2018/3/16.
 * Note:
 */
public class SecuritySupport implements SecurityService {
    @Autowired
    UserService userService;

    @Autowired
    UserAuthService userAuthService;

    public IUserAuth findUserAuthByAccount(String account) {
        return userAuthService.findUserAuthByAccount(account);
    }

    public IUser findUserByAccount(String account) {
        return userService.findUserByAccount(account);
    }


}
