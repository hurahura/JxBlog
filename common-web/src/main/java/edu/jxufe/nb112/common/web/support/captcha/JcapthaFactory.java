package edu.jxufe.nb112.common.web.support.captcha;

import com.octo.captcha.CaptchaException;
import com.octo.captcha.CaptchaQuestionHelper;
import com.octo.captcha.component.image.wordtoimage.WordToImage;
import com.octo.captcha.component.word.wordgenerator.WordGenerator;
import com.octo.captcha.image.ImageCaptcha;
import com.octo.captcha.image.gimpy.GimpyFactory;

import java.awt.image.BufferedImage;
import java.util.Locale;

/**
 * Created by lvxi on 2018/4/11.
 * Note:
 */
public class JcapthaFactory extends GimpyFactory {
    public JcapthaFactory(WordGenerator generator, WordToImage word2image){
        super(generator,word2image);
    }

    @Override
    public ImageCaptcha getImageCaptcha(Locale locale) {

        //length
        Integer wordLength = getRandomLength();

        String word = getWordGenerator().getWord(wordLength, locale);

        BufferedImage image = null;
        try {
            image = getWordToImage().getImage(word);
        } catch (Throwable e) {
            throw new CaptchaException(e);
        }

        ImageCaptcha captcha = new Jcaptcha(CaptchaQuestionHelper.getQuestion(locale, BUNDLE_QUESTION_KEY),
                image, word);
        return captcha;
    }
}
