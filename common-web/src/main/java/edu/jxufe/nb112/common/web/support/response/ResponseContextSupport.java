/*******************************************************************************
 * (c) Copyright 2017 lvxi. All Rights Reserved.
 ******************************************************************************/

package edu.jxufe.nb112.common.web.support.response;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by lvxi on 2017/2/18.
 */
public class ResponseContextSupport {
    private static final ThreadLocal<HttpServletResponse> responseLocal = new ThreadLocal<HttpServletResponse>();
    public static void setResponse(HttpServletResponse response) {
        responseLocal.set(response);
    }
    public static HttpServletResponse getResponse() {
        return (HttpServletResponse)responseLocal.get();
    }
    public static void removeResponse(){
        responseLocal.remove();
    }
}
