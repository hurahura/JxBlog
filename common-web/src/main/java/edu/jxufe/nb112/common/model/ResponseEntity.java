package edu.jxufe.nb112.common.model;

import java.io.Serializable;

public class ResponseEntity<T> implements Serializable {

    /**
     * *********************************************
     * | C O N S T A N T S |
     **********************************************
     */
    private static final long serialVersionUID = 1L;

    public static enum MessageState {

        FAILURE(0, false),
        SUCCESS(1, true),
        EXCEPTION(2, false);
        public final int index;
        public final boolean success;

        MessageState(int index, boolean success) {
            this.index = index;
            this.success = success;
        }
    }

    /**
     * *********************************************
     * | I N S T A N C E V A R I A B L E S |
     **********************************************
     */
    /**
     * 返回码
     */
    private Integer code;
    /**
     * 是否成功
     * <li> {@link MessageState#SUCCESS} 成功 {@link MessageState#FAILURE} 失败
     * {@link MessageState#EXCEPTION} 异常
     * </li>
     */
    private boolean success;
    /**
     * 返回信息
     */
    private String msg;
    /**
     * 返回结果集 用户自定义对象
     */
    private T result;

    /**
     * *********************************************
     * | C O N S T R U C T O R S |
     **********************************************
     */
    public ResponseEntity() {
    }

    public ResponseEntity(MessageState state) {
        this.code = state.index;
        this.success = state.success;
    }

    public ResponseEntity(MessageState state, String msg) {
        this.code = state.index;
        this.success = state.success;
        this.msg = msg;
    }

    public ResponseEntity(boolean success) {
        this(success,null);
    }

    public ResponseEntity(boolean success, String msg) {
        this.success = success;
        this.msg = msg;
        if(success)
            this.code = MessageState.SUCCESS.index;
        else
            this.code = MessageState.FAILURE.index;
    }

    public ResponseEntity(Integer code, boolean success, String msg) {
        this.code = code;
        this.success = success;
        this.msg = msg;
    }

    /**
     * *********************************************
     * | A C C E S S O R S / M O D I F I E R S |
     **********************************************
     */
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

}
