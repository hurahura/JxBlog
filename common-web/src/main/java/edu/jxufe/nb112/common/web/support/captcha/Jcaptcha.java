package edu.jxufe.nb112.common.web.support.captcha;

import com.octo.captcha.image.ImageCaptcha;

import java.awt.image.BufferedImage;
import java.io.Serializable;

/**
 * Created by lvxi on 2018/4/11.
 * Note:
 */
public class Jcaptcha  extends ImageCaptcha implements Serializable {
    private String answer;
    public Jcaptcha(String question, BufferedImage challenge, String answer) {
        super(question,challenge);
        this.answer=answer;
    }

    public final Boolean validateResponse(final Object response) {
        return (null != response && response instanceof String)
                ? validateResponse((String) response) : Boolean.FALSE;
    }

    private final Boolean validateResponse(final String answer) {
        return Boolean.valueOf(answer.equals(this.answer));
    }

    public String getAnswer() {
        return answer;
    }
}
