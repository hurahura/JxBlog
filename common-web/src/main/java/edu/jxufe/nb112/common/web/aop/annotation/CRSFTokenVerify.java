/*******************************************************************************
 * (c) Copyright 2017 lvxi. All Rights Reserved.
 ******************************************************************************/

package edu.jxufe.nb112.common.web.aop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by lvxi on 2018/1/31.
 */

/**
 * controller用的注解，crsfToken验证
 */
@Retention(RetentionPolicy.RUNTIME )
@Target(ElementType.METHOD)
public @interface CRSFTokenVerify {
    String value() default "";  //多个用逗号分隔
    boolean errorStop() default true; //遇到错误，不执行controller中的代码
    String errorPage() default ""; //配置后，非跳转页面
}
