/*******************************************************************************
 * (c) Copyright 2017 lvxi. All Rights Reserved.
 ******************************************************************************/

package edu.jxufe.nb112.common.web.aop.method;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

/**
 * Created by lvxi on 2017/2/5.
 */
public class LoggerAroundMethod {
    private final static  Logger LOG = LoggerFactory.getLogger(LoggerAroundMethod.class.getName());

    final private static String LINE_SPIRT="----------------------------------------------------------------";

    /**
     *在方法前后打印日志，调用耗时
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Around("execution(* edu.jxufe.nb112..*(..)) && !execution(* edu.jxufe.nb112.blog.web.support.response.RequestInterceptor..*(..))")
    public Object logAround(ProceedingJoinPoint pjp) throws Throwable {
        StopWatch stopWatch =null;
        String className =null;
        String methodName = null;
        boolean isDebugEnabled = LOG.isDebugEnabled();
        if(isDebugEnabled){
            className = pjp.getTarget().getClass().getSimpleName();
            methodName = pjp.getSignature().getName();
            StringBuilder proTip = new StringBuilder("---BEG------");
            proTip.append(className);
            proTip.append("--->>>>>>---");
            proTip.append(methodName);
            proTip.append(LINE_SPIRT);
            LOG.debug(proTip.toString());
            proTip=null;
            stopWatch = new StopWatch();
            stopWatch.start();
        }

        Object retValue = pjp.proceed();

        if(isDebugEnabled) {
            stopWatch.stop();

            StringBuilder timeTip = new StringBuilder("用时: ");
            timeTip.append(stopWatch.getTotalTimeMillis());
            timeTip.append(" ms");
            LOG.debug(timeTip.toString());

            StringBuilder postTip = new StringBuilder("---END------");
            postTip.append(className);
            postTip.append("---<<<<<<---");
            postTip.append(methodName);
            postTip.append(LINE_SPIRT);
            LOG.debug(postTip.toString());
            postTip = null;
        }
        return retValue;
    }


}
