;(function($) {
	var defaults = { //默认配置参数
		'autoFold': false, //自动收缩菜单
		'autoFoldBrotherMenu': false, //自动收缩兄弟节点子菜单
		'showMenuIcon': true, //是否显示菜单图标
		'menuIconWidth': 20, //图标宽度
		'submenuSpanWidth': 8, //子菜单与父菜单水平距离
		'showTitle': false, //添加title属性
		'menuData':null,
		'menuId':null,
		"showMenuLink":true, //是否显示菜单导航线条
		"showBorder":true
	};
	
	function AccordionMenu(selector,options){
		//顶部菜单ul
		this.mainMenu=$(selector);
		//设置
		this.setting={};
		
		$.extend(this.setting,options)
		
		if(this.setting.showMenuLink){
			this.setting.showBorder=false;
		}
		
		this.init();
	};
	
	
	AccordionMenu.prototype={
		constructor:AccordionMenu,
		//初始化
		init:function(){
			var self = this;
			var setting = this.setting;
			if(setting.menuData){
				//生成菜单
				var ul = self.createMenu(setting.menuData.menuList,0);
				self.mainMenu.append(ul);
				self.mainMenu=ul;
			}
			
			//增加菜单点击监听事件
			$("li > a",self.mainMenu).click(function(){
				$("li.active",self.mainMenu).removeClass("active");
				var liSelf=$(this).parent();
				liSelf.addClass("active");
				var ul = liSelf.children("ul"); 						
				if(setting.autoFold && (ul.size() || (setting.autoFoldBrotherMenu && liSelf.parent().find("li.open").size()))){
					$("li.open",self.mainMenu).filter(function(){
						return !liSelf.parents("li").is($(this)) && !$(this).is(liSelf);
					}).removeClass("open").children("ul").hide(200);
				}
			});
			if(!setting.showMenuIcon){
				setting.menuIconWidth=6;
			}
			//初始化下拉菜单
			this.initDropdownMenu(self.mainMenu,0);
		},
		//初始化下拉菜单
		initDropdownMenu:function(menuObj,deep){
			var self = this;
			var setting =this.setting;
			var menus = menuObj.children("li");
			menus.each(function(){
				var menuLi = $(this);
				var menu = menuLi.children("a");
				if(!setting.showBorder && !menuLi.is(self.mainMenu.children("li"))){
					menu.css("border-top-color","transparent");
				}
				var icon= menu.children("i");
				//计算多级菜单水平距离
				var vMenuSpanW = (setting.menuIconWidth+setting.submenuSpanWidth)*deep;
				menu.css("padding-left",vMenuSpanW);
	
				if(setting.showMenuIcon){
						if(icon.size()<=0) {
							vMenuSpanW = (setting.menuIconWidth)*(deep+1)+deep*setting.submenuSpanWidth;
							menu.css("padding-left",vMenuSpanW);
						}
				}else{
					icon.remove();
				}
				//title提示属性
				if(setting.showTitle){
					var $span=menu.children("span");
					$span.attr('title',$span.text());
				}
				
				//递归初始化子菜单及相关点击事件
				var submenus = menuLi.children("ul");
				if(submenus.size()>0){
					submenus.each(function(){
						var ulSelf=$(this);						
						var m = $(this).parent().children("a");

						//递归初始化
						self.initDropdownMenu($(this),deep+1);
						
						//生成横向菜单导航线条
						var lastSubMenu;
						if(setting.showMenuLink){
							var subLi = ulSelf.children("li");
							
							subLi.each(function(){
									var hlink = self.createHorizontalLink();
									hlink.css("left",vMenuSpanW+4);
									lastSubMenu=$(this).children("a");
									hlink.css("width",self.caculateHorizontalLinkWidth(lastSubMenu));
									lastSubMenu.append(hlink);
							});
							//如果最后一个子菜单下还有菜单，调整垂直导航线条的长度
							if(lastSubMenu && lastSubMenu.siblings("ul").size()>0){
								lastSubMenu.click(function(){
									var li = $(this).parent();
									var subUl = li.children("ul");
									var vlink = li.parent().siblings(".am-link-v-container");
									if(li.hasClass("open")){
										
										vlink.animate({"paddingBottom":parseInt(vlink.css("padding-bottom"))-subUl.height()},200);
										subUl.hide(200);
									}else{
										vlink.animate({"paddingBottom":parseInt(vlink.css("padding-bottom"))+subUl.height()},200);
										subUl.show(200);
									};
									li.toggleClass("open");
									setTimeout(function() {
										var pli = li.parent().parent();
										var vw= self.caculateVerticalLinkPadding(pli);
										vlink.css("padding-bottom",vw+1);
									},500);
								});
								//修正自动收缩导致垂直导航线条长度出错
								if(setting.autoFold){
									m.click(function(){
										var li = $(this).parent();
										var mb =self.caculateVerticalLinkPadding(li);
										li.children(".am-link-v-container").css("padding-bottom",mb);
									});
								}
								
								
							}
							
						}
						//点击展开事件，排除最后一个节点还有子菜单情况
						if(!setting.showMenuLink || self.mainMenu.is(menuObj) || !menuObj.children("li:last-child").is(menuLi) ){
							m.click(function(){
								if($(this).parent().hasClass("open")){
									ulSelf.hide(200);
								}else{
									ulSelf.show(200);
								};
								$(this).parent().toggleClass("open");
							});
						}
						
					});
					//生成下拉图标
					var iconDown=$('<span class="am-icon-down"></span>');
					submenus.parent().append(iconDown);
					//生成菜单级别导航线条
					if(setting.showMenuLink){
						var vlink = self.createVerticalLink();
						vlink.css("left",vMenuSpanW+3);
						menuLi.append(vlink);
					}
					
					
				}
			});
		}
		,getSubMenuData:function(menuDataList,parent){
			var subMenuData = new Array();
			for(var i=0,len=menuDataList.length;i<len;i++){
				var menuData =  menuDataList[i];
				if(menuData.parent==parent){
					subMenuData.push(menuData);
				}
			}
			return subMenuData;
		},
		createMenu:function(menuDataList,parent){
			var subMenuData = this.getSubMenuData(menuDataList,parent);
			if(subMenuData.length==0) return null;
			var ul = this.createUl();
			this.createMenuLi(ul,subMenuData);

			for(var i=0,len=subMenuData.length;i<len;i++){
				var menuData =  subMenuData[i];
				var subUl = this.createMenu(menuDataList,menuData.id);
				if(subUl!=null){
					var li =ul.children("li#am_li_"+menuData.id);
					li.append(subUl);
				}
			}
			return ul;
		},
		createMenuLi:function(ul,submenuList){
			for(var i=0,len=submenuList.length;i<len;i++){
				var menuData =  submenuList[i];
				var li = this.createLi(menuData);
				ul.append(li);
			}
		},
		createUl:function(){
			var ul = $('<ul class="accordion-menu"><ul>');
			return ul;
		},
		createLi:function(menuRow){
			var $li = $('<li><a><span></span></a></li>');
			var $span = $li.find('span');
			$span.text(menuRow.name);
			$li.attr('id',"am_li_"+menuRow.id);
			return $li;
		},
		createVerticalLink:function(){
			var vlink=$('<div class="am-link-v-container"><div class="am-link-v"></div></div>');
			return vlink;
		},
		createHorizontalLink:function(){
			var hlink=$('<div class="am-link-vr" ></div>');
			return hlink;
		},
		caculateVerticalLinkPadding:function(li){
			var h =li.height();
			var mb = 33;
			var sul =li.children("ul");
			var lastLi = sul.children("li:last-child");
			if(lastLi.size()==0) return mb;
			
			if(lastLi.hasClass("open")){
				mb+= lastLi.children("ul").height();
			}
			return mb;
		},
		caculateHorizontalLinkWidth:function(a){
			var w=5;
			if(this.setting.showMenuIcon){
				var icon = a.children("i");
				if(icon.size()==0) w+=20;
			}
			return w;
		}
		
	};

	$.fn.extend({
		'accordionMenu':function(options){
			var opts = $.extend({}, defaults , options||{});
			return new AccordionMenu(this,opts);
		}
	});
	
})(jQuery);