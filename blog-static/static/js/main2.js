(function(window, undefined) {

    _MU = window.MU;
    
    var MU = (function() {
        class2type = {
            "[object Array]": "array",
            "[object Boolean]": "boolean",
            "[object Date]": "date",
            "[object Function]": "function",
            "[object Number]": "number",
            "[object Object]": "object",
            "[object RegExp]": "regexp",
            "[object String]": "string"
        };
        /**
         * 引用对象
         */
        core_push = Array.prototype.push;
        //复制数组[begin,end)
        core_slice = Array.prototype.slice;
        core_indexOf = Array.prototype.indexOf;
        core_toString = Object.prototype.toString;
        core_hasOwn = Object.prototype.hasOwnProperty;
        core_trim = String.prototype.trim;
        var MU = function() {
            return new MU.fn.init();
        };
        MU.fn = MU.prototype = {
            constructor: MU,
            init: function() {
            }
        };
        MU.fn.init.prototype = MU.fn;
        /**
         * deep 是否深度复制
         * target 目标对象
         * source.. 源对象多个
         */
        MU.extend = MU.fn.extend = function() {
            var target = arguments[0] || {}, //目标对象
                i = 1,
                /*源对象下标*/
                deep = false,
                length = arguments.length,
                options, name, src, copy, copyIsArray, clone;
            if(typeof target === 'boolean') {
                //第一个参数为deep情况
                deep = target;
                target = arguments[i] || {};
                i++;
            }
            if(typeof target != 'object' && !MU.isFunction(target)) {
                //处理targe为 string等其他类型
                target = {};
            }
            if(length === i) {
                //只有一个参数，并且不是boolean的，目标对象为MU
                target = this;
                i--;
            }
            for(; i < length; i++) {
                if((options = arguments[i]) != null) {
                    for(name in options) {
                        src = target[name];
                        copy = options[name];
                        if(target === options) {
                            continue;
                        }
                        if(deep && copy && (MU.isPlainObject(copy) || (copyIsArray = MU.isArray(copy)))) {
                            if(copyIsArray) {
                                //复制的是数组
                                copyIsArray = false;
                                clone = src && MU.isArray(src) ? src : [];
                            } else {
                                clone = src && MU.isPlainObject(src) ? src : {};
                            }
                            target[name] = MU.extend(deep, clone, copy)
                        } else if(copy != undefined) {
                            target[name] = copy;
                        }
                    }
                }
            }
            return target;
        }
        //MU静态方法
        MU.extend({
            /**
             * 释放全局变量MU,
             */
            noConflict:function(){
                if(window.MU === MU){
                    window.MU = _MU;
                }
                return MU;
            },
            /**
             * 原始类型
             * @param {Object} obj
             */
            type: function(obj) {
                return obj == null ? new String(obj) : class2type[toString.call(obj)] || "object";
            },
            /**
             * 判断fn是不是函数
             * @param {Object} fn
             */
            isFunction: function(fn) {
                //修复ie中alert、window.open、document.getElementId typeof为object
                if("object" === typeof document.getElementById) {
                    try {
                        return /^\s*\bfunction\b/.test(obj + '');
                    } catch(e) {
                        return false;
                    }
                }
                return "[object Function]" === Object.prototype.toString.call(fn);
            },
            /**
             * obj是不是一个通过{}或new Object 创建的
             * @param {Object} obj
             */
            isPlainObject: function(obj) {
                if(MU.type(obj) !== 'object' || obj.nodeType || MU.isWindow(obj)) {
                    return false;
                }
                try {
                    if(obj.constructor && !core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
                        return false;
                    }
                } catch(e) {
                    return false;
                }
                return true;
            },
            /**
             * 判断是不是数组类型
             * @param {Object} obj
             */
            isArray: Array.isArray || function(obj) {
                console.log('isArray')
                return MU.type(obj) === "array";
            },
            /**
             * 判断是不是window对象
             * @param {Object} obj
             */
            isWindow: function(obj) {
                return obj != null && obj == obj.window; //windows对象有个属性指向自身
            },
            each: function(object, callback, args) {
                var name, i = 0,
                    length = object.length,
                    isObj = length === undefined || MU.isFunction(object);
                if(args) {
                    if(isObj) {
                        for(name in object) {
                            if(callback.apply(object[name], args) === false) {
                                break;
                            }
                        }
                    } else {
                        for(; i < length;) {
                            if(callback.apply(object[i++], args) === false) {
                                break;
                            }
                        }
                    }
                } else {
                    if(isObj) {
                        for(name in object) {
                            if(callback.call(object[name], name, object[name]) === false) {
                                break;
                            }
                        }
                    } else {
                        for(; i < length;) {
                            console.log(object[i])
                            if(callback.call(object[i], i, object[i++]) === false) {
                                break;
                            }
                        }
                    }
                }
            },
            /**
             * 获取根目录路径
             * @returns {string}
             */
            getRootPath: function (){
                var pathName = window.location.pathname.substring(1);
                var webName = pathName == '' ? '' : pathName.substring(0, pathName.indexOf('/'));
                return window.location.protocol + '//' + window.location.host + '/'+ webName + '/';
            },

            /*********************弹窗*************************************/

            /**
             * 错误消息提示弹框
             * @param message 提示消息
             * @param time 关闭时间
             * @param callback 回调函数
             */
            errorTip:function(message, time,callback) {
                if (!time) time = 3000;
                layer.msg(message, {time: time, icon: 2,shadeClose: true},callback);
            },
            /**
             * 成功消息提示弹框
             * @param message 提示消息
             * @param time 关闭时间
             * @param callback 回调函数
             */
            successTip:function (message, time,callback) {
                if (!time) t = 3000;
                layer.msg(message, {time: time, icon: 1,shadeClose: true},callback);
            },
            /********************************表单处理相关***********************************************/
            /**
             * 表单填写校验
             * @param formSelect 表单选择器
             * @returns {*|Boolean}
             */
            formCheck:function (formSelect) {
                var $formSelect = $(formSelect);
                var vlt = $formSelect.data('bootstrapValidator');
                vlt.validate();
                return vlt.isValid();
            },
            formFieldOnlineValidate:function (formSelect,fieldName) {
                var $formSelect = $(formSelect);
                var vlt = $formSelect.data('bootstrapValidator');
                //vlt.updateOption(fieldName,)

            }


        });
        /**
         * 初始化变量
         */
        MU.extend({
            version:'1.0',
            /**
             * 根目录全局变量,如果配置了就得手工改了
             */
            ctx: window.location.protocol + '//' + window.location.host+'/'
        })
        return MU;
    })();
    window.MU= MU;

})(window);