package edu.jxufe.nb112.blog.core.constant;

public interface Constant {
    /**
     * 性别常量
     */
    final static byte SEX_UNKONW = 0; //未知
    final static byte SEX_MALE = 1; //男
    final static byte SEX_FELMALE = 2; //女

}
