package edu.jxufe.nb112.blog.core.persist.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Created by lvxi on 2017/11/7.
 * Note:
 */
public class BaseDaoImpl implements  MybatisDao {

    private static final Logger LOG = LoggerFactory.getLogger(BaseDaoImpl.class.getName());

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    public SqlSessionTemplate getSqlSessionTemplate() {
        return sqlSessionTemplate;
    }

    public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
        this.sqlSessionTemplate = sqlSessionTemplate;
    }
}
