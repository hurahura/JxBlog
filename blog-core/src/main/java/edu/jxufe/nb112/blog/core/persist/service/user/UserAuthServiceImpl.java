package edu.jxufe.nb112.blog.core.persist.service.user;

import edu.jxufe.nb112.blog.core.persist.dao.UserAuthDaoImpl;
import edu.jxufe.nb112.blog.core.persist.entity.UserAuth;
import edu.jxufe.nb112.blog.core.persist.mapper.UserAuthMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by lvxi on 2017/11/8.
 * Note:
 */
@Service
public class UserAuthServiceImpl implements UserAuthService {

    private static final Logger LOG = LoggerFactory.getLogger(UserAuthServiceImpl.class.getName());

    @Autowired
    UserAuthMapper userAuthMapper;

    public UserAuth findUserAuthByAccount(String account) {
        return userAuthMapper.findUserAuthByAccount(account);
    }
}
