package edu.jxufe.nb112.blog.core.persist.service;

import edu.jxufe.nb112.blog.core.persist.entity.FtlTemplate;

/**
 * Created by lvxi on 2018/2/25.
 * Note:
 */
public interface FtlTemplateService {

   FtlTemplate getFtlTemplateById(Long id);

}
