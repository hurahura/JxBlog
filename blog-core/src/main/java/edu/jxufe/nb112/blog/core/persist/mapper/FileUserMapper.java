package edu.jxufe.nb112.blog.core.persist.mapper;

import edu.jxufe.nb112.blog.core.persist.entity.FileUser;

/**
 * Created by lvxi on 2017/12/10.
 * Note:
 */
public interface FileUserMapper {

     int insertFileUser(FileUser fileUser);

}
