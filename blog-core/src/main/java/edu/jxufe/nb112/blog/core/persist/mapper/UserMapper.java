package edu.jxufe.nb112.blog.core.persist.mapper;

import edu.jxufe.nb112.blog.core.persist.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by lvxi on 2017/11/7.
 * Note:
 */
@Mapper
public interface UserMapper {
     int insertUser(User user);
    /**
     * 通过账号查找用户
     * @param account 账号
     * @return
     */
     User findUserByAccount(String account);

    /**
     * 查找账号数量
     * @param account 账号
     * @return
     */
     Long findUserCountByAccount(String account);

    /**
     * 查找账号数量
     * @param email 账号
     * @return
     */
     Long findUserCountByEmail(String email);

    /**
     * 通过邮箱查找账号
     * @param email
     * @return
     */
     String findAccountByEmail(String email);
}
