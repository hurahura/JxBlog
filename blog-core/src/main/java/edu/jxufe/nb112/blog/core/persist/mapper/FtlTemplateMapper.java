package edu.jxufe.nb112.blog.core.persist.mapper;

import edu.jxufe.nb112.blog.core.persist.entity.FtlTemplate;

import java.util.List;

/**
 * Created by lvxi on 2017/12/10.
 * Note:
 */
public interface FtlTemplateMapper {

    FtlTemplate getFtlTemplateById(Long id);
    List<FtlTemplate> listFtlTemplate ();
}
