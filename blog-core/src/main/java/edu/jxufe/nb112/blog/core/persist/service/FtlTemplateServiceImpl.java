package edu.jxufe.nb112.blog.core.persist.service;

import edu.jxufe.nb112.blog.core.persist.entity.FtlTemplate;
import edu.jxufe.nb112.blog.core.persist.mapper.FtlTemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by lvxi on 2018/2/25.
 * Note:
 */
@Service
public class FtlTemplateServiceImpl implements FtlTemplateService {

    private static final Logger LOG = LoggerFactory.getLogger(FtlTemplateServiceImpl.class.getName());

    @Autowired
    FtlTemplateMapper ftlTemplateMapper;


    public FtlTemplate getFtlTemplateById(Long id) {
        return ftlTemplateMapper.getFtlTemplateById(id);
    }
}
