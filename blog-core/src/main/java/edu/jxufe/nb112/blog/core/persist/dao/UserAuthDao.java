package edu.jxufe.nb112.blog.core.persist.dao;

import edu.jxufe.nb112.blog.core.persist.entity.UserAuth;

/**
 * Created by lvxi on 2017/11/7.
 * Note:
 */
public interface UserAuthDao {
    public UserAuth getUserAuth(Long id);
    public int insertUserAuth(UserAuth userAuth);
}
