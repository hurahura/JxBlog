package edu.jxufe.nb112.blog.core.persist.mapper.sys;

import edu.jxufe.nb112.blog.core.persist.entity.SysConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by lvxi on 2017/11/7.
 * Note:
 */
@Mapper
public interface SysConfigMapper {
     int insertSysConfig(SysConfig sysConfig);

    /**
     * 查询sysConfig列表
     * @param params 参数条件
     * @return
     */
     List<SysConfig> listSysConfig(Map params);
}
