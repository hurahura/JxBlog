package edu.jxufe.nb112.blog.core.persist.entity;

import java.io.Serializable;
import java.util.Date;

public class LoginLog implements Serializable{
    private Long id;

    private Long userId;

    private String ip;

    private Byte result;

    private Date createTime;

    private Date updateTime;

    private Boolean valid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public Byte getResult() {
        return result;
    }

    public void setResult(Byte result) {
        this.result = result;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LoginLog loginLog = (LoginLog) o;

        if (id != null ? !id.equals(loginLog.id) : loginLog.id != null) {
            return false;
        }
        if (userId != null ? !userId.equals(loginLog.userId) : loginLog.userId != null) {
            return false;
        }
        if (ip != null ? !ip.equals(loginLog.ip) : loginLog.ip != null) {
            return false;
        }
        if (result != null ? !result.equals(loginLog.result) : loginLog.result != null) {
            return false;
        }
        if (createTime != null ? !createTime.equals(loginLog.createTime) : loginLog.createTime != null) {
            return false;
        }
        if (updateTime != null ? !updateTime.equals(loginLog.updateTime) : loginLog.updateTime != null) {
            return false;
        }
        return valid != null ? valid.equals(loginLog.valid) : loginLog.valid == null;
    }

    @Override
    public int hashCode() {
        int result1 = id != null ? id.hashCode() : 0;
        result1 = 31 * result1 + (userId != null ? userId.hashCode() : 0);
        result1 = 31 * result1 + (ip != null ? ip.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + (createTime != null ? createTime.hashCode() : 0);
        result1 = 31 * result1 + (updateTime != null ? updateTime.hashCode() : 0);
        result1 = 31 * result1 + (valid != null ? valid.hashCode() : 0);
        return result1;
    }

    @Override
    public String toString() {
        return "LoginLog{" +
                "id=" + id +
                ", userId=" + userId +
                ", ip='" + ip + '\'' +
                ", result=" + result +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", valid=" + valid +
                '}';
    }
}