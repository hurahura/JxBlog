package edu.jxufe.nb112.blog.core.persist.entity;

import edu.jxufe.nb112.common.code.security.mode.IUserAuth;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by lvxi on 2017/11/7.
 * Note:
 */
public class UserAuth implements Serializable ,IUserAuth{

    private Long id;
    private String account;
    private String password;
    private String salt;
    private Timestamp limitDate;
    private Boolean enable;
    private Boolean isCaptha;
    private Date createTime;
    private Boolean valid;

    public UserAuth() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Timestamp getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(Timestamp limitDate) {
        this.limitDate = limitDate;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Boolean getCaptha() {
        return isCaptha;
    }

    public void setCaptha(Boolean captha) {
        isCaptha = captha;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserAuth userAuth = (UserAuth) o;

        if (id != null ? !id.equals(userAuth.id) : userAuth.id != null) {
            return false;
        }
        if (account != null ? !account.equals(userAuth.account) : userAuth.account != null) {
            return false;
        }
        if (password != null ? !password.equals(userAuth.password) : userAuth.password != null) {
            return false;
        }
        if (salt != null ? !salt.equals(userAuth.salt) : userAuth.salt != null) {
            return false;
        }
        if (limitDate != null ? !limitDate.equals(userAuth.limitDate) : userAuth.limitDate != null) {
            return false;
        }
        if (enable != null ? !enable.equals(userAuth.enable) : userAuth.enable != null) {
            return false;
        }
        if (isCaptha != null ? !isCaptha.equals(userAuth.isCaptha) : userAuth.isCaptha != null) {
            return false;
        }
        if (createTime != null ? !createTime.equals(userAuth.createTime) : userAuth.createTime != null) {
            return false;
        }
        return valid != null ? valid.equals(userAuth.valid) : userAuth.valid == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (salt != null ? salt.hashCode() : 0);
        result = 31 * result + (limitDate != null ? limitDate.hashCode() : 0);
        result = 31 * result + (enable != null ? enable.hashCode() : 0);
        result = 31 * result + (isCaptha != null ? isCaptha.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (valid != null ? valid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserAuth{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", limitDate=" + limitDate +
                ", enable=" + enable +
                ", isCaptha=" + isCaptha +
                ", createTime=" + createTime +
                ", valid=" + valid +
                '}';
    }
}
