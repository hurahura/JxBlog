package edu.jxufe.nb112.blog.core.persist.dao;

import edu.jxufe.nb112.blog.core.persist.entity.UserAuth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Created by lvxi on 2017/11/7.
 * Note:
 */
@Repository
public class UserAuthDaoImpl extends BaseDaoImpl implements UserAuthDao {

    private static final Logger LOG = LoggerFactory.getLogger(UserAuthDaoImpl.class.getName());

    public UserAuth getUserAuth(Long id) {
        UserAuth userAuth = getSqlSessionTemplate().selectOne("UserAuth.getUserAuth",id);
        return userAuth;
    }

    public int insertUserAuth(UserAuth userAuth) {
        return 0;
    }
}
