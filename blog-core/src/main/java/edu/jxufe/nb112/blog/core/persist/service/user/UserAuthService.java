package edu.jxufe.nb112.blog.core.persist.service.user;

import edu.jxufe.nb112.blog.core.persist.entity.UserAuth;

/**
 * 用户相关权限服务层
 */
public interface UserAuthService {

    UserAuth findUserAuthByAccount(String account);
}
