package edu.jxufe.nb112.blog.core.constant;

/**
 * Created by lvxi on 2018/2/25.
 * Note:
 */
public interface CacheConstant {
    String ACCOUNT_CACHE="accountCache";
    //检测账号是否存在缓存
    String ACCOUNT_CACHE_ACCOUNT_PREFIX="actE_";
    //检测邮箱是否存在缓存
    String ACCOUNT_CACHE_EMAIL_PREFIX="emlE_";
}
