package edu.jxufe.nb112.blog.core.persist.service.user;

import edu.jxufe.nb112.blog.core.persist.entity.User;
import edu.jxufe.nb112.blog.core.persist.entity.UserAuth;

/**
 * 用户服务层
 */
public interface UserService {
    /**
     * 用户注册
     * @return
     */
    public boolean register(User user,UserAuth userAuth);

    /**
     * 通过账号查找用户
     * @param account 账号
     * @return
     */
    public User findUserByAccount(String account);

    /**
     * 通过邮箱查找账号
     * @param email 账号
     * @return
     */
    public String findAccountByEmail(String email);

    /**
     * 是否存在这个账号
     * @param account 账号
     * @return
     */
    public boolean hasAccount(String account);

    /**
     * 是否存在这个邮箱
     * @param email 邮箱
     * @return
     */
    public boolean hasEmail(String email);
}
