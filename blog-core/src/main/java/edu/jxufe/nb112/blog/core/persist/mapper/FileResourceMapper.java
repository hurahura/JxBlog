package edu.jxufe.nb112.blog.core.persist.mapper;

import edu.jxufe.nb112.blog.core.persist.entity.FileResource;

/**
 * Created by lvxi on 2017/12/10.
 * Note:
 */
public interface FileResourceMapper {

     int insertFileResource(FileResource fileResource);

}
