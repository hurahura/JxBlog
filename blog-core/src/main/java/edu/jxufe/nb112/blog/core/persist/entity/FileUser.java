package edu.jxufe.nb112.blog.core.persist.entity;

import java.io.Serializable;
import java.util.Date;

public class FileUser implements Serializable  {
    private Long id;

    private Long fileResourceId;

    private String name;

    private Long creator;

    private Date createTime;

    private Date updateTime;

    private Boolean valid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFileResourceId() {
        return fileResourceId;
    }

    public void setFileResourceId(Long fileResourceId) {
        this.fileResourceId = fileResourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FileUser fileUser = (FileUser) o;

        if (id != null ? !id.equals(fileUser.id) : fileUser.id != null) {
            return false;
        }
        if (fileResourceId != null ? !fileResourceId.equals(fileUser.fileResourceId) : fileUser.fileResourceId != null) {
            return false;
        }
        if (name != null ? !name.equals(fileUser.name) : fileUser.name != null) {
            return false;
        }
        if (creator != null ? !creator.equals(fileUser.creator) : fileUser.creator != null) {
            return false;
        }
        if (createTime != null ? !createTime.equals(fileUser.createTime) : fileUser.createTime != null) {
            return false;
        }
        if (updateTime != null ? !updateTime.equals(fileUser.updateTime) : fileUser.updateTime != null) {
            return false;
        }
        return valid != null ? valid.equals(fileUser.valid) : fileUser.valid == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (fileResourceId != null ? fileResourceId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (creator != null ? creator.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (valid != null ? valid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FileUser{" +
                "id=" + id +
                ", fileResourceId=" + fileResourceId +
                ", name='" + name + '\'' +
                ", creator=" + creator +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", valid=" + valid +
                '}';
    }
}