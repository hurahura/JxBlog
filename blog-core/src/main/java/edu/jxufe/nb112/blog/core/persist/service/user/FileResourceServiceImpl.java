package edu.jxufe.nb112.blog.core.persist.service.user;

import edu.jxufe.nb112.blog.core.persist.entity.FileResource;
import edu.jxufe.nb112.blog.core.persist.mapper.FileResourceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lvxi on 2017/12/10.
 * Note:
 */
@Service
public class FileResourceServiceImpl implements FileResourceService {

    private static final Logger LOG = LoggerFactory.getLogger(FileResourceServiceImpl.class.getName());

    @Autowired
    FileResourceMapper fileResourceMapper;

    @Transactional
    public Boolean saveFileResource( FileResource fileResource) {
        return fileResourceMapper.insertFileResource(fileResource)==1;
    }
}
