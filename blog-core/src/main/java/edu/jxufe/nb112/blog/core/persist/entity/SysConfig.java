package edu.jxufe.nb112.blog.core.persist.entity;

import java.io.Serializable;
import java.util.Date;

public class SysConfig implements Serializable {
    private Long id;

    private String module;

    private String keyName;

    private String dataType;

    private String keyValue;

    private Boolean enable;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private Boolean valid;

    private String  uploadPath;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SysConfig)) return false;

        SysConfig sysConfig = (SysConfig) o;

        if (id != null ? !id.equals(sysConfig.id) : sysConfig.id != null) return false;
        if (module != null ? !module.equals(sysConfig.module) : sysConfig.module != null) return false;
        if (keyName != null ? !keyName.equals(sysConfig.keyName) : sysConfig.keyName != null) return false;
        if (dataType != null ? !dataType.equals(sysConfig.dataType) : sysConfig.dataType != null) return false;
        if (keyValue != null ? !keyValue.equals(sysConfig.keyValue) : sysConfig.keyValue != null) return false;
        if (enable != null ? !enable.equals(sysConfig.enable) : sysConfig.enable != null) return false;
        if (remark != null ? !remark.equals(sysConfig.remark) : sysConfig.remark != null) return false;
        if (createTime != null ? !createTime.equals(sysConfig.createTime) : sysConfig.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(sysConfig.updateTime) : sysConfig.updateTime != null) return false;
        if (valid != null ? !valid.equals(sysConfig.valid) : sysConfig.valid != null) return false;
        return uploadPath != null ? uploadPath.equals(sysConfig.uploadPath) : sysConfig.uploadPath == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (module != null ? module.hashCode() : 0);
        result = 31 * result + (keyName != null ? keyName.hashCode() : 0);
        result = 31 * result + (dataType != null ? dataType.hashCode() : 0);
        result = 31 * result + (keyValue != null ? keyValue.hashCode() : 0);
        result = 31 * result + (enable != null ? enable.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (valid != null ? valid.hashCode() : 0);
        result = 31 * result + (uploadPath != null ? uploadPath.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SysConfig{" +
                "id=" + id +
                ", module='" + module + '\'' +
                ", keyName='" + keyName + '\'' +
                ", dataType='" + dataType + '\'' +
                ", keyValue='" + keyValue + '\'' +
                ", enable=" + enable +
                ", remark='" + remark + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", valid=" + valid +
                ", uploadPath='" + uploadPath + '\'' +
                '}';
    }
}