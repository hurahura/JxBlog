package edu.jxufe.nb112.blog.core.persist.service.user;

import edu.jxufe.nb112.blog.core.persist.entity.SysConfig;

import java.util.List;

/**
 * 系统配置服务层
 */
public interface SysConfigService {

    /**
     * 加载系统模块配置
     */
    List<SysConfig> loadSystemModuleConfig();

    /**
     * 加载模块系统配置
     * @param module
     * @param keyName
     * @return
     */
    public SysConfig getSysConfig(String module,String keyName);
    /**
     * 实例化配置的keyValue
     * @param sysConfig
     * @return 返回sysConfig.dataType类型实例化对象
     */
    Object newInstanceSysConfigValue(SysConfig sysConfig);


}
