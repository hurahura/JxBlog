package edu.jxufe.nb112.blog.core.persist.mapper;

import edu.jxufe.nb112.blog.core.persist.entity.UserAuth;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by lvxi on 2017/11/7.
 * Note:
 */
@Mapper
public interface UserAuthMapper {
    // UserAuth getUserAuth(Long id);

     int insertUserAuth(UserAuth userAuth);

    /**
     * 通过账号查找
     * @param account
     * @return
     */
     UserAuth findUserAuthByAccount(String account);
}
