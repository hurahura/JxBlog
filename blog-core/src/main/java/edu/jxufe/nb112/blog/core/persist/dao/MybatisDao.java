package edu.jxufe.nb112.blog.core.persist.dao;

import org.mybatis.spring.SqlSessionTemplate;

/**
 * Created by lvxi on 2017/11/7.
 * Note:
 */
public interface MybatisDao {
    public SqlSessionTemplate getSqlSessionTemplate();
    public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate);
}
