package edu.jxufe.nb112.blog.core.persist.service.user;

import edu.jxufe.nb112.blog.core.persist.entity.FileResource;

/**
 * Created by lvxi on 2017/12/10.
 * Note:
 */
public interface FileResourceService {
    public Boolean saveFileResource(FileResource fileResource);
}
