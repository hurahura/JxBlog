package test.edu.jxufe.nb112.blog.core.persist.service.user;

import edu.jxufe.nb112.blog.core.constant.Constant;
import edu.jxufe.nb112.blog.core.persist.entity.User;
import edu.jxufe.nb112.blog.core.persist.entity.UserAuth;
import edu.jxufe.nb112.blog.core.persist.service.user.UserService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.edu.jxufe.nb112.blog.core.BaseJunitTest;

import java.sql.Date;

/**
* UserServiceImpl Tester. 
* 
* @author lvxi
* @version 1.0 
*/ 
public class UserServiceImplTest extends BaseJunitTest {

    private static final Logger LOG = LoggerFactory.getLogger(BaseJunitTest.class.getName());

    @Autowired
    private UserService userService;
    @Before
    public void before() throws Exception { 
    
    } 
    
    @After
    public void after() throws Exception { 
    
    } 

    /** 
    * 
    * Method: register(User user, UserAuth userAuth) 
    * 
    */ 
   // @Test
    public void testRegister() throws Exception {

        UserAuth userAuth = new UserAuth();
        userAuth.setAccount("test");
        userAuth.setPassword("1234");
        userAuth.setSalt("salt");

        User user = new User();
        user.setAccount("test");
        Date birthday = Date.valueOf("2017-11-08");
        user.setBirthday(birthday);
        user.setNickname("Just dot it");
        user.setPhoto("ppp");
        user.setSex(Constant.SEX_UNKONW);
        user.setPhone("12345678910");
        user.setEmail("a@b.com");

        Assert.assertTrue(userService.register(user,userAuth));

    }

   // @Test
    public void testFindUserByAccount() throws Exception {
        User user = userService.findUserByAccount("test");
        Assert.assertNotNull(user);
        Assert.assertEquals("test",user.getAccount());
    }
}
