package test.edu.jxufe.nb112.blog.core.persist.mapper;

import edu.jxufe.nb112.blog.core.persist.entity.UserAuth;
import edu.jxufe.nb112.blog.core.persist.mapper.UserAuthMapper;
import edu.jxufe.nb112.blog.core.persist.service.user.UserServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import test.edu.jxufe.nb112.blog.core.BaseJunitTest;

public class UserAuthMapperTest extends BaseJunitTest {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class.getName());

    @Autowired
    private UserAuthMapper userAuthMapper;

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    //@Test
    @Transactional
    public void testInsertUserAuth(){
        UserAuth userAuth = new UserAuth();
        userAuth.setAccount("test");
        userAuth.setPassword("1234");
        userAuth.setSalt("salt");
        int result = userAuthMapper.insertUserAuth(userAuth);
        Assert.assertTrue(result==1);

    }


} 
