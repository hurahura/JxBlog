package test.edu.jxufe.nb112.blog.core.persist.mapper;

import edu.jxufe.nb112.blog.core.constant.Constant;
import edu.jxufe.nb112.blog.core.persist.mapper.UserMapper;
import edu.jxufe.nb112.blog.core.persist.entity.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import test.edu.jxufe.nb112.blog.core.BaseJunitTest;

import java.sql.Date;


public class UserMapperTest extends BaseJunitTest {

    @Autowired
    private UserMapper userMapper;

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    //@Test
    @Transactional
    public void testInsertUser(){
        User user = new User();
        user.setAccount("test");
        Date birthday = Date.valueOf("2017-11-08");
        user.setBirthday(birthday);
        user.setNickname("Just dot it");
        user.setPhoto("ppp");
        user.setSex(Constant.SEX_UNKONW);
        user.setPhone("12345678910");
        user.setEmail("a@b.com");
        int result = userMapper.insertUser(user);
        Assert.assertTrue(result==1);

    }


} 
