package test.edu.jxufe.nb112.blog.core.persist.mapper;

import edu.jxufe.nb112.blog.core.persist.entity.SysConfig;
import edu.jxufe.nb112.blog.core.persist.mapper.sys.SysConfigMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.edu.jxufe.nb112.blog.core.BaseJunitTest;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lvxi on 2017/11/18.
 * Note:
 */
public class SysConfigMapperTest extends BaseJunitTest  {


    private static final Logger LOG = LoggerFactory.getLogger(SysConfigMapperTest.class.getName());

    @Autowired
    private SysConfigMapper sysConfigMapper;

   // @Test
    public void insertSysConfig() throws Exception {
        SysConfig sysConfig = new SysConfig();
        sysConfig.setEnable(true);
        sysConfig.setDataType("String");
        sysConfig.setModule("system");
        sysConfig.setKeyName("domain");
        sysConfig.setKeyValue("http://localhost:8080");
        sysConfig.setRemark("系统网站域名");
        sysConfigMapper.insertSysConfig(sysConfig);
        Assert.assertNotNull(sysConfig.getId());
    }

    @Test
    public void listSysConfig() throws Exception {
        HashMap params = new HashMap();
        params.put("module","system");
        params.put("enable",true);
        List<SysConfig> result = sysConfigMapper.listSysConfig(params);
    }

    @Test
    public void testReflectClass() {
        try {
            Class onwClass = Class.forName("java.lang.String");
            Constructor c = onwClass.getConstructor(String.class);
            Object object = c.newInstance("fdsafads");
            System.out.println(object);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}