package test.edu.jxufe.nb112.blog.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lvxi on 2017/2/5.
 */
@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试
@ContextConfiguration({"classpath*:/config/spring/spring-core.xml"}) //加载配置文件
@Rollback(value = false) //提交事务
@Transactional(transactionManager = "transactionManager")  //事务管理器
public class BaseJunitTest {
    private static final Logger LOG = LoggerFactory.getLogger(BaseJunitTest.class.getName());

    @Test
    public void test(){
        LOG.debug("ttt中文{}字符测试{}","1","2");
        LOG.debug("ttt");
        LOG.error("error测试");
        LOG.trace("trace测试");
    }
}
