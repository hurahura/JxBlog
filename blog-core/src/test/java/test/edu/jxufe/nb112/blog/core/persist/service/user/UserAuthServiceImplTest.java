package test.edu.jxufe.nb112.blog.core.persist.service.user; 

import edu.jxufe.nb112.blog.core.persist.entity.UserAuth;
import edu.jxufe.nb112.blog.core.persist.service.user.UserAuthServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import test.edu.jxufe.nb112.blog.core.BaseJunitTest;
/** 
* UserAuthServiceImpl Tester. 
* 
* @author lvxi
* @version 1.0 
*/ 
public class UserAuthServiceImplTest extends BaseJunitTest { 
    @Autowired
    private UserAuthServiceImpl userAuthServiceImpl;
    @Before
    public void before() throws Exception { 
    
    } 
    
    @After
    public void after() throws Exception { 
    
    } 

    /** 
    * 
    * Method: findUserAuthByAccount(String account) 
    * 
    */ 
   // @Test
    public void testFindUserAuthByAccount() throws Exception {
        UserAuth userAuth =userAuthServiceImpl.findUserAuthByAccount("test");
        Assert.assertNotNull(userAuth);
        userAuth =userAuthServiceImpl.findUserAuthByAccount("test123");
        Assert.assertNull(userAuth);
    } 


} 

