package test.edu.jxufe.nb112.blog.core.persist.service.user;

import edu.jxufe.nb112.blog.core.persist.entity.SysConfig;
import edu.jxufe.nb112.blog.core.persist.service.user.SysConfigService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.edu.jxufe.nb112.blog.core.BaseJunitTest;

import java.util.List;

/**
* SysConfigServiceImpl Tester. 
* 
* @author lvxi
* @version 1.0 
*/ 
public class SysConfigServiceImplTest extends BaseJunitTest {

    private static final Logger LOG = LoggerFactory.getLogger(SysConfigServiceImplTest.class.getName());

    @Autowired
    private SysConfigService sysConfigService;
    @Before
    public void before() throws Exception { 
    
    } 
    
    @After
    public void after() throws Exception { 
    
    } 

    /** 
    * 
    * Method: loadSystemModuleConfig() 
    * 
    */ 
    @Test
    public void testLoadSystemModuleConfig() throws Exception {
        List<SysConfig> configs = sysConfigService.loadSystemModuleConfig();
        configs = sysConfigService.loadSystemModuleConfig();
        configs = sysConfigService.loadSystemModuleConfig();
        configs = sysConfigService.loadSystemModuleConfig();
    } 

    /** 
    * 
    * Method: newInstanceSysConfigValue(SysConfig sysConfig) 
    * 
    */ 
    @Test
    public void testNewInstanceSysConfigValue() throws Exception { 
     
    } 


} 
