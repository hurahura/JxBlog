<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<link href="${ctx}/plugin/bootstrap/css/bootstrap.min.css?5057f321f0dc85cd8da94a0c5f67a8f4" rel="stylesheet">
<link href="${ctx}/plugin/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">

<!--图标库-->
<%--<link href="//at.alicdn.com/t/font_asutmntbhyupiudi.css" rel="stylesheet">--%>
<!--bootstrapr日期控件-->
<link href="${ctx}/plugin/datepick/bootstrap-datetimepicker.min.css" rel="stylesheet">

<link href="${ctx}/css/main.less" rel="stylesheet/less" type="text/css" />
<link href="${ctx}/css/iconfont.less" rel="stylesheet/less" type="text/css" />
<!--[if lt IE 9]>
<script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!--jquery-->
<script src="${ctx}/plugin/jquery/jquery-1.11.3.min.js?13c0a5055cca7b2463b2f73701960b9e"></script>

<!--angularjs-->
<script src="${ctx}/plugin/angular.min.js" type="text/javascript"></script>

<!--jquery-ui Begin-->
<link href="${ctx}/plugin/jquery-ui-1.12.1.custom/jquery-ui.css"  rel="stylesheet"/>
<script src="${ctx}/plugin/jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
<script src="${ctx}/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<!--jquery-ui End-->

<!--jqGrid Begin-->
<link rel="stylesheet" type="text/css" href="${ctx}/plugin/jqGrid-5.2.0/css/ui.jqgrid-bootstrap.css"/>
<!--<link rel="stylesheet" type="text/css" href="plugin/jqGrid-5.2.0/css/ui.jqgrid.css"/>-->
<script src="${ctx}/plugin/jqGrid-5.2.0/js/i18n/grid.locale-cn.js" type="text/javascript" charset="utf-8"></script>
<script src="${ctx}/plugin/jqGrid-5.2.0/js/jquery.jqGrid.min.js" type="text/javascript" charset="utf-8"></script>
<!--jqGrid End-->
<!--bootstrapValidator begin-->
<link type="text/css" rel="stylesheet" href="${ctx}/plugin/bootstrap-validator/css/bootstrapValidator.css" />
<script type="text/javascript" src="${ctx}/plugin/bootstrap-validator/js/bootstrapValidator.js"></script>
   <script type="tttext/javascript" src="${ctx}/plugin/bootstrap-validator/js/language/zh_CN.js"></script>
<!--bootstrapValidator end-->

<!--bootstrap switch begin-->
<link rel="stylesheet跨行"
      href="${ctx}/plugin/bootstrap-switch/bootstrap-switch.min.css" />
<!--bootstrap switch end-->

<!--<link rel="单行注释stylesheet----" href="${ctx}/plugin/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css" />
<!--
<link rel="跨行注释--stylesheet" href="${ctx}/plugin/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css" />
-->
<%--
<link rel="跨行注释%stylesheet" href="${ctx}/plugin/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css" />
--%>

<%--
<link rel="多行跨行注释%stylesheet" href="${ctx}/plugin/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css" />
<link rel="多行跨行注释%stylesheet" href="${ctx}/plugin/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css" />

--%>
