<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script src="${ctx}/plugin/bootstrap/js/bootstrap.min.js?04c84852e9937b142ac73c285b895b85"></script>
<script src="${ctx}/plugin/bootstrap-table/bootstrap-table.js"></script>
<script src="${ctx}/plugin/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>

<script src="${ctx}/plugin/datepick/bootstrap-datetimepicker.min.js"></script>
<script src="${ctx}/plugin/datepick/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="${ctx}/plugin/bootstrap-switch/bootstrap-switch-3.3.4.js" type="text/javascript"></script>

<script src="${ctx}/plugin/malihu-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<!--less-->
<script src="${ctx}/plugin/less.min.js" type="text/javascript"></script>
<script src="${ctx}/js/main.js"></script>
