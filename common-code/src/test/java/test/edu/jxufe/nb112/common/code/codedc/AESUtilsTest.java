package test.edu.jxufe.nb112.common.code.codedc; 

import edu.jxufe.nb112.common.code.codedc.AESUtils;
import edu.jxufe.nb112.common.code.codedc.HexUtils;
import edu.jxufe.nb112.common.code.io.ZipUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;

/**
* AESUtils Tester. 
* 
* @author lvxi
* @version 1.0 
*/ 
public class AESUtilsTest   {

    private static final Logger LOG = LoggerFactory.getLogger(AESUtilsTest.class.getName());

    @Before
    public void before() throws Exception { 
    
    } 
    
    @After
    public void after() throws Exception {

    }
    public static String binary(byte[] bytes, int radix){
        return new BigInteger(1, bytes).toString(radix);// 这里的1代表正数
    }

    /** 
    * 
    * Method: encode(String in) 
    * 
    */ 
    @Test
    public void testEncodeIn() throws Exception {
        byte[] bytes =  HexUtils.hex2Byte("123456住校吗");
        String str = binary(bytes,32);
        System.out.println(str);
    } 

    /** 
    * 
    * Method: encode(String in, String password) 
    * 
    */ 
    @Test
    public void testEncodeForInPassword() throws Exception { 
     
    } 

    /** 
    * 
    * Method: decode(String hex) 
    * 
    */ 
    @Test
    public void testDecodeHex() throws Exception { 
     
    } 

    /** 
    * 
    * Method: decode(String hex, String password) 
    * 
    */ 
    @Test
    public void testDecodeForHexPassword() throws Exception { 
     
    } 

    /** 
    * 
    * Method: encodeTimestap(String in, long liveTime) 
    * 
    */ 
    @Test
    public void testEncodeTimestapForInLiveTime() throws Exception { 
     
    } 

    /** 
    * 
    * Method: encodeTimestap(String in, long liveTime, String password) 
    * 
    */ 
    @Test
    public void testEncodeTimestapForInLiveTimePassword() throws Exception { 
     
    } 

    /** 
    * 
    * Method: decodeTimestap(String hex) 
    * 
    */ 
    @Test
    public void testDecodeTimestapHex() throws Exception {
        String str =AESUtils.encode("123456");
        System.out.println("密文"+str+"  "+str.length());

        String content = AESUtils.decode(str);
        System.out.println(content);
        //48775856fb5db9a777429e52f5c5ada4
    } 

    /** 
    * 
    * Method: decodeTimestap(String hex, String password) 
    * 
    */ 
    @Test
    public void testDecodeTimestapForHexPassword() throws Exception {
        String str =AESUtils.encodeTimestap("123456",100);
        System.out.println("密文"+str+" "+str.length());
        String zipstr = ZipUtils.gzip(str);
        System.out.println("压缩后密文"+zipstr+"  "+zipstr.length());
        String content = AESUtils.decode(str);
        System.out.println(content);
    } 


} 
