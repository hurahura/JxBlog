package test.edu.jxufe.nb112.common.code.io; 

import edu.jxufe.nb112.common.code.io.FileUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

/**
* FileUtils Tester. 
* 
* @author lvxi
* @version 1.0 
*/ 
public class FileUtilsTest {

    private static final Logger LOG = LoggerFactory.getLogger(FileUtilsTest.class.getName());

    @Before
    public void before() throws Exception { 
    
    } 
    
    @After
    public void after() throws Exception { 
    
    } 

    /** 
    * 
    * Method: listAll(final File file, FileFilter fileFilter) 
    * 
    */ 
    @Test
    public void testListAll() throws Exception {
        List<File> files = FileUtils.listAll(new File("E:/Workspace/Work/JxBlog"));
        for (File file :files){
            System.out.println(file.getAbsolutePath());
        }
    }

    /** 
    * 
    * Method: listAllFiles(final File file, final FileFilter fileFilter) 
    * 
    */ 
    @Test
    public void testListAllFiles() throws Exception {
        List<File> files = FileUtils.listAllFiles(new File("JxBlog"),new RegexFileFilter(".*java"));
        for (File file :files){
            System.out.println(file.getAbsolutePath());
        }
    }

    @Test
    public void testListAllDirectory() throws Exception {
        List<File> files = FileUtils.listAllDirectory(new File("E:/Workspace/Work/JxBlog"),null);
        for (File file :files){
            System.out.println(file.getAbsolutePath());
        }
    }

} 
