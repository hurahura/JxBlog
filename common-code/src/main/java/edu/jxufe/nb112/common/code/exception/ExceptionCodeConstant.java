package edu.jxufe.nb112.common.code.exception;

/**
 * Created by lvxi on 2017/11/19.
 * Note:
 */
public interface ExceptionCodeConstant {
    String DEFAULT="exception.default";

    String SYS_CONFIG="exception.system.config";

    String SYS_SHIRO_SESSION="exception.shiro.session";
}
