package edu.jxufe.nb112.common.code.mode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lvxi on 2018/4/13.
 * Note:
 */
public class JsonResponse extends HashMap {
    final private static String KEY_DATA = "data";
    final private static String KEY_CODE = "code";
    final private static String KEY_STATUS = "status";
    final private static String KEY_MSG = "msg";
    final private static String KEY_ERROR_FIELDS = "fields"; //字段检验错误

    //private HashMap<String,Object> jsonMap = new HashMap<String,Object>();

    public JsonResponse() {

    }

    public JsonResponse(Boolean status) {
        this.setStatus(status);
    }

    public JsonResponse setFields(String[] fields) {
        //标示校验错误
        super.put(KEY_CODE,"1");
        super.put(KEY_ERROR_FIELDS, fields);
        //链式调用
        return this;
    }


    public JsonResponse setCode(String code) {
        super.put(KEY_CODE, code);
        //链式调用
        return this;
    }


    public JsonResponse setStatus(Boolean status) {
        super.put(KEY_STATUS, status);
        return this;
    }


    public JsonResponse setMessage(Object msg) {
        super.put(KEY_MSG, msg.toString());
        return this;
    }

    private void preInitData(){
        if (!super.containsKey(KEY_DATA)) {
            super.put(KEY_DATA, new HashMap<String, Object>());
        }
    }

    public HashMap<String, Object> getData(){
        return (HashMap<String, Object>) super.get(KEY_DATA);
    }

    public Object getData(String key){
        preInitData();
        return getData().get(key);
    }

    public <T> T getData(String key,Class<T> tClass){
        preInitData();
        return (T) getData().get(key);
    }


    public JsonResponse putData(String key, Object value) {
        getData().put(key, value);
        return this;
    }

    /**
     * 实现链式调用
     *
     * @param map
     * @return
     */
    public JsonResponse addAllData(Map<? extends String, ?> map) {
        getData().putAll(map);
        return this;
    }

    public JsonResponse put(String key, Object value) {
        super.put(key, value);
        return this;
    }


    public JsonResponse addAll(Map m) {
        super.putAll(m);
        return this;
    }

    @Override
    public JsonResponse remove(Object key) {
        super.remove(key);
        return this;
    }

    public JsonResponse clearAll() {
        super.clear();
        return this;
    }

    public static JsonResponse create() {
        JsonResponse jsonResponse = new JsonResponse();
        return jsonResponse;
    }

    public static JsonResponse create(Boolean status) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus(status);
        return jsonResponse;
    }

    public static JsonResponse create(Boolean status, String code) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus(status);
        if (code != null) {
            jsonResponse.setCode(code);
        }
        return jsonResponse;
    }

    public static JsonResponse create(Boolean status, String code, Object msg) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus(status);
        if (code != null) {
            jsonResponse.setCode(code);
        }
        if (msg != null) {
            jsonResponse.setMessage(msg);
        }
        return jsonResponse;
    }

    public static JsonResponse create(Boolean status, String code, Object msg, Map data) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus(status);
        if(code != null) {
            jsonResponse.setCode(code);
        }
        if(msg != null) {
            jsonResponse.setMessage(msg);
        }
        if(data !=null) {
            jsonResponse.addAllData(data);
        }
        return jsonResponse;
    }

    public static JsonResponse create(Boolean status, Map data) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus(status);
        jsonResponse.addAllData(data);
        return jsonResponse;
    }

    public static JsonResponse create(Boolean status, String msg, Map data) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus(status);
        if (msg != null) {
            jsonResponse.setMessage(msg);
        }
        if (data != null) {
            jsonResponse.addAllData(data);
        }
        return jsonResponse;
    }

    public static JsonResponse create(Map data) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.addAllData(data);
        return jsonResponse;
    }

    /**
     * 成功状态
     *
     * @return
     */
    public static JsonResponse success() {
        return JsonResponse.create(Boolean.TRUE);
    }

    /**
     * 成功状态
     *
     * @return
     */
    public static JsonResponse success(String code) {
        return JsonResponse.create(Boolean.TRUE,code);
    }

    /**
     * 成功状态
     *
     * @return
     */
    public static JsonResponse success(String code, Object msg) {
        return JsonResponse.create(Boolean.TRUE,code).setMessage(msg);
    }

    /**
     * 成功状态
     *
     * @return
     */
    public static JsonResponse success(Map data) {
        return JsonResponse.create(data).setStatus(Boolean.TRUE);
    }

    /**
     * 成功状态
     *
     * @return
     */
    public static JsonResponse success(String msg, Map data) {
        return JsonResponse.create(data).setStatus(Boolean.TRUE).setMessage(msg);
    }


    /**
     * 失败状态
     *
     * @return
     */
    public static JsonResponse fail() {
        return JsonResponse.create(Boolean.FALSE);
    }

    /**
     * 失败状态
     *
     * @return
     */
    public static JsonResponse fail(String code) {
        return JsonResponse.create(Boolean.FALSE,code);
    }

    /**
     * 失败状态
     *
     * @return
     */
    public static JsonResponse fail(String code, Object msg) {
        return JsonResponse.create(Boolean.FALSE,code).setMessage(msg);
    }

    /**
     * 失败状态
     *
     * @return
     */
    public static JsonResponse fail(Map data) {
        return  JsonResponse.create(data).setStatus(Boolean.FALSE);
    }

    /**
     * 失败状态
     *
     * @return
     */
    public static JsonResponse fail(String msg, Map data) {
        return JsonResponse.create(data).setMessage(msg).setStatus(Boolean.FALSE);
    }

}
