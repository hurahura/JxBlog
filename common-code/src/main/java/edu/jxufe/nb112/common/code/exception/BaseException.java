package edu.jxufe.nb112.common.code.exception;

/**
 * Created by lvxi on 2017/11/19.
 * Note:
 */
public class BaseException extends  RuntimeException{



    /**
     * message key
     */
    private String code;

    /**
     * message params
     */
    private Object[] values;

    public BaseException(){

    }
    public BaseException(Object value){
        code = ExceptionCodeConstant.DEFAULT;
        values= new Object[]{value};
    }
    public BaseException(Throwable cause){
        super(cause);
    }
    public BaseException(String message, Throwable cause){
        super(message, cause);
    }

    public BaseException(String message, Throwable cause, String code, Object[] values) {
        super(message, cause);
        this.code = code;
        this.values = values;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the values
     */
    public Object[] getValues() {
        return values;
    }

    /**
     * @param values the values to set
     */
    public void setValues(Object[] values) {
        this.values = values;
    }



}
