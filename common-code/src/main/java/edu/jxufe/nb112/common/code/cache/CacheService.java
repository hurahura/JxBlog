package edu.jxufe.nb112.common.code.cache;

import org.springframework.cache.Cache;

import java.util.Collection;
import java.util.Set;

/**
 * Created by lvxi on 2018/2/25.
 * Note:
 */
public interface CacheService<K,V> {
    /**
     * 获取缓存
     * @param cacheName
     * @return
     */
    Cache getCache(String cacheName);

    /**
     * 获取缓存
     * @param cacheName
     * @param key
     * @return
     */
    Object get(String cacheName,Object key);

    /**
     * 清除缓存
     * @param cacheName 缓存名称
     * @param key 主键
     */
    void evict(String cacheName, Object key);

    /**
     * 清空缓存
     * @param cacheName
     */
    void clear(String cacheName);

    /**
     * 获取缓存数量
     * @param cacheName
     * @return
     */
    int size(String cacheName);


    /**
     * 获取所有key
     * @param cacheName
     * @return
     */
    Set<K> keys(String cacheName);

    /**
     * 获取所有值
     * @param cacheName
     * @return
     */
    Collection<V> values(String cacheName);
}
