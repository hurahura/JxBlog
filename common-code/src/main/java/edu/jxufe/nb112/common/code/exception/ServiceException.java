package edu.jxufe.nb112.common.code.exception;

/**
 * Created by lvxi on 2017/11/19.
 * Note:
 */
public class ServiceException extends BaseException {

    public ServiceException() {
        super(ExceptionCodeConstant.DEFAULT,null,ExceptionCodeConstant.DEFAULT, null);
    }

    /**
     * Constructors
     *
     * @param code
     *            错误代码
     */
    public ServiceException(String code) {
        super(code,null,code, null);
    }

    /**
     * Constructors
     *
     * @param cause
     *            异常接口
     * @param code
     *            错误代码
     */
    public ServiceException(Throwable cause, String code) {
        super(code, cause, code, null);
    }

    /**
     * Constructors
     *
     * @param code
     *            错误代码
     * @param values
     *            一组异常信息待定参数
     */
    public ServiceException(String code, Object[] values) {
        super(code, null, code, values);
    }

    /**
     * Constructors
     *
     * @param code
     *            错误代码
     * @param value
     *            信息待定参数
     */
    public ServiceException(String code, Object value) {
        super(code, null, code, new Object[]{value});
    }

    /**
     * Constructors
     *
     * @param cause
     *            异常接口
     * @param code
     *            错误代码
     * @param values
     *            一组异常信息待定参数
     */
    public ServiceException(Throwable cause, String code, Object[] values) {
        super(code,null , code, values);
    }
}
