package edu.jxufe.nb112.common.code.security.mode;

import java.sql.Timestamp;

/**
 * Created by lvxi on 2018/3/16.
 * Note:
 */
public interface IUserAuth {
    /**
     * 获取账号
     * @return
     */
    public String getAccount();

    /**
     * 获取密码
     * @return
     */
    public String getPassword();

    /**
     * 获取加密的盐
     * @return
     */
    public String getSalt();

    /**
     * 获取账号被禁言的日期
     * @return
     */
    public Timestamp getLimitDate();

    /**
     * 账号是否可用
     * @return
     */
    public Boolean getEnable();

}
