package edu.jxufe.nb112.common.code.exception;

/**
 * Created by lvxi on 2017/11/19.
 * Note:
 */
public class CommonCodeException extends BaseException {

    public CommonCodeException() {
        super();
    }

    public CommonCodeException(Throwable throwable) {
        super(throwable);
    }

    public CommonCodeException(String message, Throwable cause){
        super(message, cause);
    }


}
