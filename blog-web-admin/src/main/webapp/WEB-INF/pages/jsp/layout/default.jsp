<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><sitemesh:write property='title' /> - ${_system_domain_name}</title>
    <link href="/static/favicon.ico" rel="shortcut icon">
    <%-- nginx-http-concat模块合并 css/js
    <link href="/static/plugin/font-awesome-4.7.0/css/font-awesome.min.css"  rel="stylesheet" type="text/css"/>
    <link href="/static/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/static/plugin/bootstrap-validator/css/bootstrapValidator.css" rel="stylesheet" type="text/css"/>
    <link href="/static/plugin/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    --%>
    <link href="/static/plugin??font-awesome-4.7.0/css/font-awesome.min.css,bootstrap/css/bootstrap.min.css,bootstrap-validator/css/bootstrapValidator.css,bootstrap-fileinput/css/fileinput.min.css"  rel="stylesheet" type="text/css" />
    <link href="/static/css/main.css" rel="stylesheet" type="text/css" />
    <script src="/static/plugin/jquery/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="/static/js/main.js" type="text/javascript"></script>
    <sitemesh:write property='head' />
</head>
<body>
    <div class="ms-lyt-background" >
        <img src="/static/img/bg.jpg"/>
    </div>
    <header>
    <nav class="navbar navbar-default mb0">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand ms-lyt-brand" href="/" style="padding: 0px">
                    <img src="/static/img/logo.png" height="50px">
                    <img src="/static/img/blog.png" height="30px">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">首页</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">文章<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                    <li><a href="/">关于</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right ms-navbar">
                    <shiro:guest>
                    <li><a href="/login" class="inline-block pr5">登录</a>/<a href="/register" class="inline-block pl5">注册</a></li>
                    </shiro:guest>
                    <shiro:user>
                        <li>
                            <a href="#" title="消息" >
                                <i class="fa fa-bell-o fa-lg">
                                    <span class="badge" >45</span>
                                </i>

                            </a>
                        </li>
                        <li class="dropdown">
                            <c:set var="nickname"><c:out value="${userProfile.nickname}" /></c:set>
                            <a href="#" class="dropdown-toggle btn-img" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <img src="/static/img/head.jpg"  alt="${nickname}" title="${nickname}" class="img-rounded">
                            </a>
                            <ul class="dropdown-menu ms-icon-menu">
                                <li><div class="text-center text-oneline">${nickname}</div></li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="#"><i class="glyphicon glyphicon-user"></i>个人中心</a>
                                </li>
                                <li>
                                    <a href="#"><i class="glyphicon glyphicon-cog"></i>个人设置</a>
                                </li>
                                <shiro:hasAnyRoles name="root">
                                <li>
                                    <a href="#"><i class="glyphicon glyphicon-cog"></i>后台管理</a>
                                </li>
                                </shiro:hasAnyRoles>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="/logout"><i class="glyphicon glyphicon-off"></i>安全退出</a>
                                </li>
                            </ul>
                        </li>
                    </shiro:user>
                </ul>

            </div>


        </div>
    </nav>
    </header>

    <div id="mainBody" class="container pl0 pr0">
        <div class="panel panel-default radius0 border0">
            <div class="panel-body">
                <sitemesh:write property='body' />
            </div>
        </div>
    </div>

    <footer>
        <div id="copyright"  class="text-center">(c) Copyright 2017 jxufe nb112. All Rights Reserved.</div>
    </footer>
    <script src="/static/plugin/??bootstrap/js/bootstrap.min.js,bootstrap-validator/js/bootstrapValidator.js,bootstrap-validator/js/language/zh_CN.js,layer/layer.js" type="text/javascript" ></script>
    <%--
    <script src="/static/plugin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/static/plugin/bootstrap-validator/js/bootstrapValidator.js" type="text/javascript"></script>
    <script src="/static/plugin/bootstrap-validator/js/language/zh_CN.js" type="text/javascript"></script>
    <script src="/static/plugin/layer/layer.js" type="text/javascript"></script>
    --%>
    <%--
    <script src="/static/plugin/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
    <script src="/static/plugin/bootstrap-fileinput/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="/static/plugin/bootstrap-fileinput/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="/static/plugin/bootstrap-fileinput/js/fileinput.js"></script>
    <script src="/static/plugin/bootstrap-fileinput/themes/fa/theme.js"></script>
    <script src="/static/plugin/bootstrap-fileinput/js/locales/zh.js"></script>
    --%>
</body>
</html>