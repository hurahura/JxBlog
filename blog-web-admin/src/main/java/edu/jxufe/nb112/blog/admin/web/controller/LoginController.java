package edu.jxufe.nb112.blog.admin.web.controller;

import edu.jxufe.nb112.blog.core.persist.service.user.UserService;
import edu.jxufe.nb112.common.code.mode.JsonResponse;
import edu.jxufe.nb112.common.web.aop.annotation.UserDataVerify;
import edu.jxufe.nb112.common.web.support.captcha.JcaptchaService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by lvxi on 2018/1/21.
 * Note:
 */
@Controller
@RequestMapping
public class LoginController extends BaseController {

    private final static Logger LOG = LoggerFactory.getLogger(LoginController.class.getName());
    @Autowired
    UserService userService;

    @Autowired
    private JcaptchaService jcaptchaService;
    /**
     * 登录提交
     * @return
     */
    @RequestMapping(value = "login" ,method = RequestMethod.POST)
    @UserDataVerify
    @ResponseBody
    public JsonResponse loginSubmit(@RequestParam(value = "account") String account, @RequestParam(value = "password") String password,@RequestParam(value = "verify") String verify){

        JsonResponse ret = new JsonResponse(false);
        String captchaId = getRequest().getSession().getId();
        //校验验证码
        boolean isOk = jcaptchaService.validateResponseForID("login"+":"+captchaId,verify);
        if(!isOk){
            ret.setFields(new String[]{"verify"});
            return ret;
        }
        if(account.contains("@")){
            //邮箱登录
            account = userService.findAccountByEmail(account);
        }

        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(account, password);
        try {
            subject.login(token);
            ret.setStatus(true);
        }catch (LockedAccountException e) {
            String msg="账号已被锁定,禁止登录";
            if(StringUtils.isNotEmpty(e.getMessage())){
                msg="账号已被锁定,禁止登录,"+e.getMessage()+"后解锁";
            }
            ret.setMessage(msg);
        }catch (ExcessiveAttemptsException e){
            ret.setMessage("多次登录失败，账号已被锁定,"+e.getMessage()+"后解锁");
        }
        catch (AuthenticationException e) {
            ret.setMessage("用户名密码错误,登录失败");
        }
        return ret;
    }

    /**
     * 退出
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(){
        SecurityUtils.getSubject().logout();
        return redirect("/");
    }

}
